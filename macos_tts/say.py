import os
import sys
import logging_helper
from fdutil.safe import file_safe
from cachingutil.binary_file_cache import (BinaryFileCache,
                                           CacheError)

logging = logging_helper.setup_logging()

# TODO: Add constants for macOS say, e.g. voices, bit rate, channels etc


class VoiceError(Exception):
    pass


class TextToSpeech(object):

    def __init__(self,
                 string=None,
                 **params
                 # TODO: Figure out macOS say parameters required
                 ):
        """

        :param string: The text to speak on the command line.
                        This can consist of multiple arguments, which are
                        considered to be separated by spaces.

        :param params:
            input_file=file
             f file
                 A file to be spoken.
                 If file is - or neither this parameter nor
                 a message is specified, read from standard input.

             file_format=format
                 The format of the file to write (AIFF, caff, m4af, WAVE).
                 Generally, it's easier to specify a suitable file extension
                 for the output file. To obtain a list of writable file formats,
                 specify '?' as the format name.

             data_format=format
                 The format of the audio data to be stored. default=linear PCM.

             rate=rate
             r rate
                 Speech rate to be used, in words per minute.

             voice=voice
             v voice
                 The voice to be used: Alex, Bruce, Fred, Kathy, Vicki or
                 Victoria. Default is the voice selected in
                 System Preferences | Speech

             output_file=fileout.aiff
             o fileout.aiff
                 An AIFF file to be written, some voices support other file
                 formats.

             channels=channels
                 The number of channels. Most synthesizers produce mono audio
                 only.

             bit_rate=rate
                 The bit rate for formats, default=AAC.specify '?' as the rate.

             quality=quality
                 The audio converter quality level between 0 (lowest)
                 and 127 (highest).
   """
        if sys.platform != u'Darwin':
            raise EnvironmentError(u'macos_say_utils can only be run on macOS')

        self.string = string
        self.params = params

        self.filepath = self.params.get(u'output-file',
                                        self.params.get(u'o'))

        # Converts dict to list of parameters names and values matching the
        # macOS command line format. As all parameters use '-' in the names,
        # and none use '_', replace '_' with '-'.  this lets us supply all
        # the parameters we want in call.

        params = [(u"--{name}={value}"
                   if len(key) > 1
                   else u'-{name} {v}'
                   ).format(name=key.replace(u'_', u'-'),
                            value=value)
                  for key, value in params.iteritems()]

        self.say = (u'say {params} {string}'
                    .format(string=string,
                            params=u' '.join(params)))
        os.system(self.say)

    @property
    def speech(self):
        with self.filepath.open(self.filepath, mode=u'rb') as binary_file:
            return binary_file.read()

    def save(self,
             filepath):
        pass
        # TODO: save filedata to file

    def play(self):
        print([ord(c) for c in self.speech])
        # TODO: figure out how to play the sound


class TextToSpeechCache(BinaryFileCache):

    def __init__(self,
                 folder,
                 file_format,
                 # TODO: Figure out parameters
                 ):

        self.folder = folder
        self.file_format = file_format
        # TODO store parameters
        super(TextToSpeechCache, self).__init__(folder=folder)

    def filename(self,
                 string):
        string = file_safe(string.strip().lower())
        filename = u'{string}.{ext}'.format(string=string,
                                            ext=self.file_format.lower())
        return os.path.join(self.folder,
                            filename)

    def fetch_from_source(self,
                          string,
                          **params):
        try:
            tts = TextToSpeech(string = string,
                               output_file=self.filename(string=string),
                               **params)
            return tts.speech
        except Exception as e:
            logging.exception(e)
            raise CacheError(u'Could not fetch from source (ERROR: {error})'
                             .format(error = e.message))
